//! Crops of family Asteraceae

use derive_getters::Getters;

use super::{Age, Crop, Family};
use crate::{conditions, settings::Language};

/// Garden salad
#[derive(Debug, Clone, PartialEq, Eq, Default, Getters)]
pub struct GardenSalad {
    age: Age,
    variety: Option<String>,
}

impl GardenSalad {
    /// Sow a new salad
    pub fn sow(variety: Option<String>) -> Self {
        GardenSalad {
            age: Age::Seedling,
            variety,
        }
    }
    /// Plant family
    pub fn family() -> Family {
        Family::Asteraceae
    }
    /// Localized name
    pub fn name(lang: Language) -> &'static str {
        match lang {
            Language::English => "garden salad",
            Language::German => "Gartensalat",
        }
    }
    /// Scientific name
    pub fn name_sci() -> &'static str {
        "Lactuca sativa"
    }
    /// Light range to grow in
    pub fn light() -> conditions::LightConditions {
        let mut conditions = conditions::LightConditions::new();
        conditions.insert(conditions::Light::LightShade);
        conditions.insert(conditions::Light::SummerShade);
        conditions.insert(conditions::Light::Sun);
        conditions.insert(conditions::Light::WinterShade);
        conditions
    }
    /// Temperature range to grow in
    pub fn temperature(age: Age) -> conditions::TemperatureRange {
        match age {
            Age::Seedling => {
                conditions::Temperature::Celsius(10.0)..=conditions::Temperature::Celsius(16.0)
            }
            _ => conditions::Temperature::Celsius(-5.0)..=conditions::Temperature::Celsius(35.0),
        }
    }
}

impl Crop for GardenSalad {
    fn family(&self) -> Family {
        Self::family()
    }
    fn name(&self, lang: Language) -> &'static str {
        Self::name(lang)
    }
    fn name_sci(&self) -> &'static str {
        Self::name_sci()
    }
    fn variety(&self) -> &Option<String> {
        Self::variety(self)
    }
    fn light(&self) -> conditions::LightConditions {
        Self::light()
    }
    fn age(&self) -> &Age {
        self.age()
    }
    fn set_age(&mut self, age: Age) {
        self.age = age
    }
    fn temperature(&self) -> conditions::TemperatureRange {
        Self::temperature(self.age)
    }
}
