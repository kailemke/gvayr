//! Fields with blocks of beds
use std::fmt::Display;

use crate::conditions;

use derive_getters::Getters;
use derive_more::{Add, Div, From, Into, Mul, Sum};

/// A garden bed
/// ```
/// use gvayr::{conditions::Light, field::{Area, Bed, Length}};
///
/// let bed = Bed::new(
///     "Test Bed".to_string(),
///     100.into(), 50.into(),
///     Light::Sun, Some("Test Block".to_string()),
/// );
/// assert_eq!(
///     format!("{}", bed),
///     "Test Bed: 0.5 m² (1 m x 50 cm), sunny, block: Test Block"
/// );
/// assert_eq!(Length::from(100), bed.length());
/// assert_eq!(Length::from(50), bed.width());
/// assert_eq!(Area::from(0.5), bed.area());
///```
#[derive(Debug, PartialEq, Eq, Clone, Getters, Hash)]
pub struct Bed {
    /// A name to address the  bed
    name: String,
    /// Bed dimension
    dimensions: Dimensions,
    /// Light conditions
    light: conditions::Light,
    /// Optional block to be associated with
    block: Option<Block>,
}

impl Bed {
    /// Create a new bed
    ///
    /// # Arguments
    /// * `name` - A name to address the bed
    /// * `length` - The bed length in cm
    /// * `width` - The bed width in cm
    /// * `light` - The bed's light conditions
    /// * `block` - The block this bed is associated with
    pub fn new(
        name: String,
        length: Length,
        width: Length,
        light: conditions::Light,
        block: Option<Block>,
    ) -> Self {
        let dimensions = Dimensions::new(length, width);
        Self {
            name,
            dimensions,
            light,
            block,
        }
    }
    /// Get the bed length
    pub fn length(&self) -> Length {
        self.dimensions.length
    }
    /// Get the bed width
    pub fn width(&self) -> Length {
        self.dimensions.width
    }
    /// Get the bed area in m²
    pub fn area(&self) -> Area {
        Area::from(self.dimensions)
    }
}

impl Display for Bed {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}: {}, {}, block: {}",
            self.name,
            self.dimensions,
            self.light,
            self.block.clone().unwrap_or("None".to_string())
        )
    }
}

/// A length, distance or spacing
///
/// The base unit of storage is cm, but m will be displayed if suitable.
/// ```
/// use gvayr::field::Length;
///
/// assert_eq!("0 cm".to_string(), format!("{}", Length::from(0)));
/// assert_eq!("42 cm".to_string(), format!("{}", Length::from(42)));
/// assert_eq!("1 m".to_string(), format!("{}", Length::from(100)));
/// assert_eq!("1.42 m".to_string(), format!("{}", Length::from(142)));
/// ```
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, From, Into, Add, Div, Mul, Sum)]
pub struct Length(u16);

impl Display for Length {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.0 < 100 {
            return write!(f, "{} cm", self.0);
        }
        write!(f, "{}", Meter::from(*self))
    }
}

#[derive(Debug, PartialEq, Clone, Copy, From, Into, Add, Div, Mul, Sum)]
struct Meter(f32);

impl From<Length> for Meter {
    fn from(value: Length) -> Self {
        let cm: u16 = value.into();
        let m = f32::from(cm) / 100.0;
        m.into()
    }
}

impl Display for Meter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} m", self.0)
    }
}

/// Dimensions of a rectangle
///
/// ```
/// use gvayr::field::Dimensions;
///
/// let d = Dimensions::new(42.into(), 42.into());
/// assert_eq!(0.17639999, f32::from(d.area()));
///
/// let d = Dimensions::new(800.into(), 75.into());
/// assert_eq!("6 m² (8 m x 75 cm)".to_string(), format!("{}", d));
/// ```
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Dimensions {
    pub length: Length,
    pub width: Length,
}

impl Dimensions {
    /// Construct new Dimensions
    pub fn new(length: Length, width: Length) -> Self {
        Dimensions { length, width }
    }
    /// Get the area of the rectangle
    pub fn area(&self) -> Area {
        Area::from(*self)
    }
}

impl Display for Dimensions {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} ({} x {})", self.area(), self.length, self.width)
    }
}

/// An area in m²
///
/// ```
/// use gvayr::field::{Area, Dimensions};
///
/// assert_eq!("0 m²".to_string(), format!("{}", Area::from(0.0)));
/// assert_eq!("1 m²".to_string(), format!("{}", Area::from(1.0)));
/// let d = Dimensions::new(42.into(), 42.into());
/// assert_eq!("0.17639999 m²".to_string(), format!("{}", Area::from(d)));
/// ```
#[derive(Debug, PartialEq, Clone, Copy, From, Into, Add, Div, Mul, Sum)]
pub struct Area(f32);

impl Display for Area {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} m²", self.0)
    }
}

impl From<Dimensions> for Area {
    fn from(value: Dimensions) -> Self {
        let length = Meter::from(value.length).0;
        let width = Meter::from(value.width).0;
        Self::from(length * width)
    }
}

/// A block of beds
pub type Block = String;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bed_comparison() {
        let bed1 = Bed::new(
            "Bed 1".to_string(),
            100.into(),
            50.into(),
            conditions::Light::Sun,
            None,
        );
        let bed2 = bed1.clone();
        let bed3 = Bed::new(
            "Bed 3".to_string(),
            200.into(),
            50.into(),
            conditions::Light::Sun,
            None,
        );
        let bed4 = Bed::new(
            "Bed 4".to_string(),
            100.into(),
            75.into(),
            conditions::Light::Sun,
            None,
        );
        let bed5 = Bed::new(
            "Bed 5".to_string(),
            100.into(),
            50.into(),
            conditions::Light::LightShade,
            None,
        );
        let bed6 = Bed::new(
            "Bed 5".to_string(),
            100.into(),
            50.into(),
            conditions::Light::LightShade,
            Some("Test Block".to_string()),
        );
        assert_eq!(bed1, bed2);
        assert_ne!(bed1, bed3);
        assert_ne!(bed1, bed4);
        assert_ne!(bed1, bed5);
        assert_ne!(bed5, bed6);
    }

    #[test]
    fn convert_length_to_meter() {
        assert_eq!(Meter::from(Length(42)), Meter(0.42));
    }
}
