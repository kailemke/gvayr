//! Errors of gvayr
use thiserror::Error;

/// Error
#[derive(Debug, Error, PartialEq, Eq)]
pub enum Error {
    #[error("The field block name '{0}' is already taken")]
    DuplicateBlockName(String),
    #[error("The bed name '{0}' is already taken")]
    DuplicateBedName(String),
    #[error("The field block '{0}' is not found")]
    BlockNotFound(String),
}
