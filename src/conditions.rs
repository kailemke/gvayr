//! Growing conditions
use std::collections::HashSet;
use std::fmt::Display;
use std::ops::RangeInclusive;

use enum_iterator::Sequence;
pub use simmer::Temperature;

/// Light conditions of a bed
#[derive(Debug, PartialEq, Eq, Clone, Copy, Sequence, Hash)]
pub enum Light {
    /// A sunny bed
    Sun,
    /// A bed in light shade
    LightShade,
    /// A bed that is shaded in the summer months
    SummerShade,
    /// A bed that is shaded in the winter months
    WinterShade,
    /// A bed in full shade
    Shade,
}

impl Display for Light {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Self::Sun => "sunny",
            Self::LightShade => "light shaded",
            Self::SummerShade => "summer shaded",
            Self::WinterShade => "winter shaded",
            Self::Shade => "shaded",
        };
        write!(f, "{}", s)
    }
}

/// Light conditions to grow in
pub type LightConditions = HashSet<Light>;

/// Temperature range
pub type TemperatureRange = RangeInclusive<Temperature>;

#[cfg(test)]
mod tests {
    use super::*;

    use enum_iterator::all;

    #[test]
    fn test_light_iterator() {
        assert_eq!(
            all::<Light>().collect::<Vec<Light>>(),
            [
                Light::Sun,
                Light::LightShade,
                Light::SummerShade,
                Light::WinterShade,
                Light::Shade
            ]
        );
    }
}
