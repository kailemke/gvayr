pub mod conditions;
pub mod crop;
pub mod error;
pub mod field;
pub mod settings;

use error::Error;

use std::collections::HashSet;

use derive_getters::Getters;

/// The actual gvayr application
#[derive(Debug, Clone, Getters, Default)]
pub struct App {
    /// Beds
    beds: HashSet<field::Bed>,
    /// Field blocks
    field_blocks: HashSet<String>,
}

impl App {
    /// Create a new gvayr application
    pub fn new() -> Self {
        App::default()
    }
    /// Find a bed by name
    pub fn find_bed(&self, bed_name: &str) -> Option<&field::Bed> {
        self.beds.iter().find(|bed| bed.name() == bed_name)
    }
    /// Add a new bed
    ///
    /// # Arguments
    /// * `name` - The bed name
    /// * `length` - The bed length in cm
    /// * `width` - The bed width in cm
    /// * `light` - The bed's light conditions
    /// * `block` - The optional block the bed should be associated
    ///
    /// # Returns
    /// - If the name is free `Ok(())` is returned.
    /// - If there is already a bed with that name, `Err(Error::DuplicateBedName(name))` is returned.
    /// - If the given field block name does not exist, `Err(Error:BlockNotFound(block))` is returned.
    pub fn new_bed(
        &mut self,
        name: String,
        length: field::Length,
        width: field::Length,
        light: conditions::Light,
        block: Option<field::Block>,
    ) -> Result<String, Error> {
        if self.find_bed(&name).is_some() {
            return Err(Error::DuplicateBedName(name));
        }
        if let Some(block) = block.clone() {
            if !self.field_blocks.contains(&block) {
                return Err(Error::BlockNotFound(block));
            }
        }
        let bed = field::Bed::new(name.clone(), length, width, light, block);
        self.beds.insert(bed);
        Ok(name)
    }
    /// Add a new field block
    ///
    /// # Arguments
    /// * `name` - The block name
    ///
    /// # Returns
    /// Returns `Err(Error::DuplicateBlockName(name))` if there is already a block with that name,
    /// `Ok(name)` otherwise.
    pub fn new_field_block(&mut self, name: String) -> Result<String, Error> {
        match self.field_blocks.insert(name.clone()) {
            true => Ok(name),
            false => Err(Error::DuplicateBlockName(name)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_app() {
        let app = App::new();
        assert!(app.beds().is_empty());
        assert!(app.field_blocks().is_empty());
    }

    #[test]
    fn test_new_field_block() {
        let mut app = App::new();
        assert_eq!(
            Ok("First Block".to_string()),
            app.new_field_block("First Block".to_string())
        );
        assert_eq!(
            "The field block name 'First Block' is already taken".to_string(),
            app.new_field_block("First Block".to_string())
                .unwrap_err()
                .to_string()
        );
        assert_eq!(
            Ok("Second Block".to_string()),
            app.new_field_block("Second Block".to_string())
        );
        assert_eq!(2, app.field_blocks().len());
    }

    #[test]
    fn test_new_bed() {
        let mut app = App::new();
        assert_eq!(
            Ok("Standalone Bed".to_string()),
            app.new_bed(
                "Standalone Bed".to_string(),
                800.into(),
                75.into(),
                conditions::Light::Sun,
                None,
            )
        );
        assert_eq!(
            "The bed name 'Standalone Bed' is already taken".to_string(),
            app.new_bed(
                "Standalone Bed".to_string(),
                900.into(),
                75.into(),
                conditions::Light::Sun,
                None
            )
            .unwrap_err()
            .to_string()
        );
        assert_eq!(
            "The field block 'Test Bed' is not found".to_string(),
            app.new_bed(
                "Test Bed 1".to_string(),
                800.into(),
                75.into(),
                conditions::Light::Sun,
                Some("Test Bed".to_string()),
            )
            .unwrap_err()
            .to_string()
        );
        assert_eq!(
            Ok("Test Bed".to_string()),
            app.new_field_block("Test Bed".to_string())
        );
        assert_eq!(
            Ok("Test Bed 1".to_string()),
            app.new_bed(
                "Test Bed 1".to_string(),
                800.into(),
                75.into(),
                conditions::Light::Sun,
                Some("Test Bed".to_string()),
            )
        );
        assert_eq!(2, app.beds().len());
        let bed_names = app
            .beds()
            .iter()
            .map(|bed| bed.name().as_str())
            .collect::<HashSet<&str>>();
        assert!(bed_names.contains("Standalone Bed"));
        assert!(bed_names.contains("Test Bed 1"));
    }
}
