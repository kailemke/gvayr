//! Crops
use std::fmt::Display;

use enum_iterator::{next, Sequence};

use crate::{conditions, settings::Language};

use self::asteraceae::GardenSalad;

pub mod asteraceae;

/// Vegetables to grow
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Vegetable {
    GardenSalad(GardenSalad),
}

/// A crop
pub trait Crop {
    /// Plant family
    fn family(&self) -> Family;

    /// Localized name
    fn name(&self, lang: Language) -> &'static str;

    /// Scientific name
    fn name_sci(&self) -> &'static str;

    /// Variety / Breed / Cultivar
    fn variety(&self) -> &Option<String>;

    /// Localized summary
    ///
    /// ```
    /// use gvayr::{
    ///     conditions::Light,
    ///     crop::Crop,
    ///     crop::asteraceae::GardenSalad,
    ///     settings::Language,
    /// };
    ///
    /// let red_velvet = GardenSalad::sow(Some("Red Velvet".to_string()));
    /// assert_eq!(
    ///     "Gartensalat (Lactuca sativa, Asteraceae) 'Red Velvet'".to_string(),
    ///     red_velvet.summary(Language::German)
    /// );
    /// let anonymous = GardenSalad::sow(None);
    /// assert_eq!(
    ///     "Gartensalat (Lactuca sativa, Asteraceae) ".to_string(),
    ///     anonymous.summary(Language::German)
    /// );
    /// ```
    fn summary(&self, lang: Language) -> String {
        format!(
            "{} ({}, {}) {}",
            self.name(lang),
            self.name_sci(),
            self.family(),
            self.variety()
                .clone()
                .map_or("".to_string(), |variety_name| format!("'{}'", variety_name)),
        )
    }

    /// Light range to grow in
    fn light(&self) -> conditions::LightConditions;

    /// Crop age
    fn age(&self) -> &Age;

    /// Temperature range to grow in
    fn temperature(&self) -> conditions::TemperatureRange;

    /// Is it OK to grow the crop in this  conditions?
    ///
    /// ```
    /// use gvayr::{crop::Crop, crop::asteraceae::GardenSalad, conditions};
    /// use enum_iterator::all;
    ///
    /// let salad = GardenSalad::sow(None);
    /// for light in all::<conditions::Light>() {
    ///     if light == conditions::Light::Shade {
    ///         continue;
    ///     }
    ///     assert!(salad.conditions_ok(&light, &conditions::Temperature::Celsius(16.0)));
    /// }
    /// ```
    fn conditions_ok(
        &self,
        light: &conditions::Light,
        temperature: &conditions::Temperature,
    ) -> bool {
        self.light().contains(light) && self.temperature().contains(temperature)
    }

    /// Set the plant age
    fn set_age(&mut self, age: Age);

    /// Let the plant age
    ///
    /// ```
    /// use gvayr::crop::{Age, Crop, asteraceae::GardenSalad};
    ///
    /// let mut salad = GardenSalad::sow(None);
    /// salad.grow();
    /// assert_eq!(&Age::Youvenil, salad.age());
    /// salad.grow();
    /// assert_eq!(&Age::Maturing, salad.age());
    /// salad.grow();
    /// assert_eq!(&Age::Ready, salad.age());
    /// salad.grow();
    /// assert_eq!(&Age::Reproducing, salad.age());
    /// salad.grow();
    /// assert_eq!(&Age::Overaged, salad.age());
    /// salad.grow();
    /// assert_eq!(&Age::Overaged, salad.age());
    /// ```
    fn grow(&mut self) {
        match self.age() {
            Age::Overaged => (),
            _ => self.set_age(next(self.age()).unwrap()),
        }
    }
}

/// Plant family
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Family {
    Aizoaceae,
    Alliaceae,
    Amaranthaceae,
    Apiaceae,
    Asteraceae,
    Brassicaceae,
    Cucurbitaceae,
    Fabaceae,
    Lamiaceae,
    Montiaceae,
    Polygonaceae,
    Solanaceae,
    Tropaeolaceae,
    Violaceae,
}

impl Display for Family {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// Plant age
#[derive(Debug, Clone, Copy, PartialEq, Eq, Sequence)]
pub enum Age {
    /// A seedling
    Seedling,
    /// A young plant more or less ready to transplnt
    Youvenil,
    /// A plant growing outdoors, but still maturing
    Maturing,
    /// A plant ready to harvest
    Ready,
    /// A plant that is reproducing
    Reproducing,
    /// A plant that is (as good as) dead
    Overaged,
}

impl Display for Age {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Default for Age {
    fn default() -> Self {
        Self::Seedling
    }
}
